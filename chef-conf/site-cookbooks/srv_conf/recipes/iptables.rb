#
# Cookbook Name:: srv_conf
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash "iptables_rules_copy" do
  code <<-EOL

  #sudo iptables -F
  #sudo iptables -t nat -F
  #sudo iptables -t mangle -F
  #iptables -X
  #sudo iptables -P INPUT DROP
  #sudo iptables -P OUTPUT ACCEPT
  #sudo iptables -P FORWARD ACCEPT
  #sudo iptables -A INPUT -i eth1 -p tcp --dport 80 -j ACCEPT
  #sudo iptables -A INPUT -i eth1 -p tcp --dport 443 -j ACCEPT
  #sudo iptables -A INPUT -i eth1 -p tcp --dport 2222 -j ACCEPT
  #sudo iptables -A INPUT -p icmp -j ACCEPT 
  if [ ! -d /etc/iptables ]; then
	sudo mkdir /etc/iptables
  fi
  #sudo /sbin/iptables-save > /etc/iptables/general
  sudo cp /vagrant_data/iptables/general /etc/iptables/general -f && sudo /sbin/iptables-restore < /etc/iptables/general
  EOL
end
