#
# Cookbook Name:: srv_conf
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash "vhost_copy" do
  code <<-EOL
  sudo cp /vagrant_data/sites/sites-available/default /etc/nginx/sites-available/ -f
  sudo /etc/init.d/nginx restart
  EOL
end

bash "install_php_7.1" do
  code <<-EOL
  sudo add-apt-repository ppa:ondrej/php -y
  sudo apt-get update && sudo apt-get install -y php7.1 php7.1-fpm php7.1-mcrypt php-geoip php-redis
  sudo /etc/init.d/php7.1-fpm start
  sudo update-rc.d php7.1-fpm defaults  
  EOL
end
