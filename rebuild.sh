#!/bin/bash
#cd srv
#vagrant reload --provision
cd chef-conf
berks update && berks install
knife solo bootstrap vagrant@test.srv -i ~/.vagrant.d/insecure_private_key
